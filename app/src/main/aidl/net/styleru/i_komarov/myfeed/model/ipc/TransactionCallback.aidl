// TransactionCallback.aidl
package net.styleru.i_komarov.myfeed.model.ipc;

import net.styleru.i_komarov.myfeed.model.dto.TransactionEvent;

interface TransactionCallback {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onTransactionEvent(in TransactionEvent transactionEvent);
}
