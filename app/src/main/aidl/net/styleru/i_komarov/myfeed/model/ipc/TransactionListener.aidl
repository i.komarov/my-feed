// TransactionListener.aidl
package net.styleru.i_komarov.myfeed.model.ipc;

import net.styleru.i_komarov.myfeed.model.ipc.TransactionCallback;
import net.styleru.i_komarov.myfeed.model.dto.TransactionEntity;

interface TransactionListener {

    void addCallback(in TransactionCallback callback, in long identifier);

    void executeTransaction(in TransactionEntity transaction, in long identifier);
}
