package net.styleru.i_komarov.myfeed.app;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import net.styleru.i_komarov.myfeed.model.service.TransactionService;

/**
 * Created by i_komarov on 28.11.16.
 */

public class FeedApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Intent transactionServiceIntent = new Intent(this.getApplicationContext(), TransactionService.class);
        startService(transactionServiceIntent);
        Log.d("FeedApp", "service started from FeedApp");
    }
}
