package net.styleru.i_komarov.myfeed.common.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by i_komarov on 22.11.16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViewComponents(this);
    }

    protected abstract void setupViewComponents(Context context);
}
