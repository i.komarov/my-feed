package net.styleru.i_komarov.myfeed.common.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by i_komarov on 24.11.16.
 */

public abstract class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViewComponents(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    protected abstract void setupViewComponents(View view);
}
