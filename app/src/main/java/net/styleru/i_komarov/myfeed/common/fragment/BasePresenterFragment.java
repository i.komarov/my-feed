package net.styleru.i_komarov.myfeed.common.fragment;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import net.styleru.i_komarov.myfeed.common.presenter.RetainedPresenter;
import net.styleru.i_komarov.myfeed.presenter.cache.IPresenterFactory;
import net.styleru.i_komarov.myfeed.presenter.cache.PresenterLoader;

/**
 * Created by i_komarov on 26.11.16.
 */

public abstract class BasePresenterFragment<V, P extends RetainedPresenter<V>> extends BaseFragment implements LoaderManager.LoaderCallbacks<P> {

    private boolean DEBUG;
    protected P presenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getLoaderManager().initLoader(getPresenterLoaderId(), null, this);
    }

    @Override
    public Loader<P> onCreateLoader(int id, Bundle args) {
        Log.d("BasePresenterFragment", "onCreateLoader() called");
        return new PresenterLoader<>(getActivity().getApplicationContext(), getPresenterFactory());
    }

    @Override
    public void onLoadFinished(Loader<P> loader, P presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onLoaderReset(Loader<P> loader) {
        this.presenter = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        //at this point we may be sure that presenter has bee loaded, so now we may do some stuff with it
        //presenter.onStart();
        //presenter.bindView(getIView());
        /*if(DEBUG) {
            Log.d("BasePresenterFragment", "onStart(), presenter.onStart() and presenter.bindView() called");
        }
        onPresenterInitialized();
        */
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onStart();
        presenter.bindView(getIView());
        onPresenterInitialized();
    }

    @Override
    public void onStop() {
        presenter.unbindView();
        presenter.onStop();
        if(DEBUG) {
            Log.d("BasePresenterFragment", "onStop(), presenter.unbind() and presenter.onStop() called");
        }
        super.onStop();
    }

    protected abstract int getPresenterLoaderId();

    protected abstract IPresenterFactory<V, P> getPresenterFactory();

    protected abstract void onPresenterInitialized();

    protected abstract V getIView();

    public void setDebug(boolean debug) {
        this.DEBUG = debug;
    }
}
