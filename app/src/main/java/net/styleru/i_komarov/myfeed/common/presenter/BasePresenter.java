package net.styleru.i_komarov.myfeed.common.presenter;

/**
 * Created by i_komarov on 23.11.16.
 */

public abstract class BasePresenter<T> {

    protected T view;

    public BasePresenter() {

    }

    public void bindView(T view) {
        this.view = view;
    }

    public void unbindView() {
        this.view = null;
    }

    public T getView() {
        return this.view;
    }
}
