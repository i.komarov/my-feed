package net.styleru.i_komarov.myfeed.common.presenter;

/**
 * Created by i_komarov on 24.11.16.
 */

public interface IBasePresenter<V> {

    void onStart();

    void onStop();

    void onDestroy();

    void bindView(V view);

    void unbindView();
}
