package net.styleru.i_komarov.myfeed.common.presenter;

import net.styleru.i_komarov.myfeed.presenter.cache.IPresenterFactory;

/**
 * Created by i_komarov on 26.11.16.
 */

public abstract class RetainedPresenter<V> implements IBasePresenter<V> {

    protected V view;

    public RetainedPresenter() {

    }

    @Override
    public void bindView(V view) {
        this.view = view;
    }

    @Override
    public void unbindView() {
        this.view = null;
    }
}
