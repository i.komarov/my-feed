package net.styleru.i_komarov.myfeed.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 27.11.16.
 */

public class TransactionEntity implements Parcelable {

    private Long id;
    private String content;

    public TransactionEntity() {

    }

    public TransactionEntity(Parcel in) {
        id = in.readLong();
        content = in.readString();
    }

    public TransactionEntity(Long id, String content) {
        this.id = id;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(content);
    }

    public static final Creator<TransactionEntity> CREATOR = new Creator<TransactionEntity>() {
        @Override
        public TransactionEntity createFromParcel(Parcel in) {
            return new TransactionEntity(in);
        }

        @Override
        public TransactionEntity[] newArray(int size) {
            return new TransactionEntity[size];
        }
    };
}
