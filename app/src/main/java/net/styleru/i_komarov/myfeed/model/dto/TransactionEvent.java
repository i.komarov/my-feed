package net.styleru.i_komarov.myfeed.model.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 27.11.16.
 */

public class TransactionEvent implements Parcelable {

    public static int SUCCESS = 0;
    public static int FAILURE = 1;
    public static int UNKNOWN = 2;

    private long id;
    private int status;

    public TransactionEvent() {

    }

    public TransactionEvent(Parcel in) {
        this.id = in.readLong();
        this.status = in.readInt();
    }

    public TransactionEvent(long id, int status) {
        this.id = id;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(status);
    }

    public static Parcelable.Creator<TransactionEvent> CREATOR = new Creator<TransactionEvent>() {

        @Override
        public TransactionEvent createFromParcel(Parcel in) {
            return new TransactionEvent(in);
        }

        @Override
        public TransactionEvent[] newArray(int size) {
            return new TransactionEvent[0];
        }
    };

    @Override
    public String toString() {
        return "TransactionEvent{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }
}
