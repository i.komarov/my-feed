package net.styleru.i_komarov.myfeed.model.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LongSparseArray;

import net.styleru.i_komarov.myfeed.model.dto.TransactionEntity;
import net.styleru.i_komarov.myfeed.model.dto.TransactionEvent;
import net.styleru.i_komarov.myfeed.model.ipc.TransactionCallback;
import net.styleru.i_komarov.myfeed.model.ipc.TransactionListener;
import net.styleru.i_komarov.myfeed.model.util.DataWrapper;

import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by i_komarov on 27.11.16.
 */

public class TransactionService extends Service {

    private static final int TRANSACTION_EXECUTE_RETRY_COUNT = 3;

    private final RemoteCallbackList<TransactionCallback> callbacks = new RemoteCallbackList<>();

    private LongSparseArray<TransactionEvent> transactionResultCache = new LongSparseArray<>();

    private IBinder binder;

    private DisposableSubscriber<DataWrapper<TransactionEntity, Long>> transactionSubscriber = new DisposableSubscriber<DataWrapper<TransactionEntity, Long>>() {
        @Override
        public void onNext(DataWrapper<TransactionEntity, Long> transactionData) {
            executePendingTransaction(transactionData);
        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onComplete() {

        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("TransactionService", "onBind, binder is " + (binder == null ? "null" : "not null"));

        if(binder == null) {
            setupTransactionListenerChannel();
        }

        return binder;
    }

    @Override
    public void onCreate() {
        Log.d("TransactionService", "onCreate");
        startForeground(1, new Notification());
        if(binder == null) {
            setupTransactionListenerChannel();
        }
    }

    @Override
    public void onDestroy() {
        Log.d("TransactionService", "onDestroy");
        if(!transactionSubscriber.isDisposed()) {
            transactionSubscriber.dispose();
        }
    }

    private void executePendingTransaction(DataWrapper<TransactionEntity, Long> transaction) {
        Log.d("TransactionService", "executePendingTransaction");
        //TODO: do some heavy stuff here
        TransactionEvent event = new TransactionEvent(transaction.getData().getId(), TransactionEvent.SUCCESS);

        TransactionCallback callback = getRecepientByID(transaction.getExtra());
        if(callback != null) {
            try {
                callback.onTransactionEvent(event);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            callbacks.finishBroadcast();
        } else {
            //adding result to cache, as no callback has the id similar to this
            transactionResultCache.put(transaction.getExtra(), event);
            Log.d("TransactionService", "no callback found and thus event cached: " + transaction.getExtra() + ", " + transaction.getData());
            //TODO: show notification here
        }
    }

    private void setupTransactionListenerChannel() {
        Flowable.create(
                new FlowableOnSubscribe<DataWrapper<TransactionEntity, Long>>() {
                    @Override
                    public void subscribe(FlowableEmitter<DataWrapper<TransactionEntity, Long>> transactionEmitter) throws Exception {
                        binder = new TransactionListener.Stub() {
                            @Override
                            public void addCallback(TransactionCallback callback, long identifier) throws RemoteException {
                                if(callback != null) {
                                    if(transactionResultCache.get(identifier) != null) {
                                        callback.onTransactionEvent(transactionResultCache.get(identifier));
                                        transactionResultCache.remove(identifier);
                                    } else {
                                        callbacks.register(callback, identifier);
                                    }
                                }
                            }

                            @Override
                            public void executeTransaction(TransactionEntity transaction, long identifier) throws RemoteException {
                                transactionEmitter.onNext(new DataWrapper<>(transaction, identifier));
                            }
                        };
                    }
                },
                BackpressureStrategy.BUFFER
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .delay(15, TimeUnit.SECONDS)
                .retry(TRANSACTION_EXECUTE_RETRY_COUNT)
                .subscribe(
                        //transactionSubscriber
                );
    }

    private TransactionCallback getRecepientByID(long targetID) {

        callbacks.beginBroadcast();
        for(int i = 0; i < callbacks.getRegisteredCallbackCount(); i++) {
            long id = (long) callbacks.getBroadcastCookie(i);
            if(id == targetID) {
                return callbacks.getBroadcastItem(i);
            }
        }

        callbacks.finishBroadcast();

        return null;
    }
}
