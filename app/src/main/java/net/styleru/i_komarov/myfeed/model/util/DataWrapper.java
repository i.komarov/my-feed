package net.styleru.i_komarov.myfeed.model.util;

/**
 * Created by i_komarov on 28.11.16.
 */

public class DataWrapper<T, E> {

    private T data;
    private E extra;


    public DataWrapper(T data, E extra) {
        this.data = data;
        this.extra = extra;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public E getExtra() {
        return extra;
    }

    public void setExtra(E extra) {
        this.extra = extra;
    }
}
