package net.styleru.i_komarov.myfeed.presenter.cache;

import net.styleru.i_komarov.myfeed.common.presenter.RetainedPresenter;

/**
 * Created by i_komarov on 26.11.16.
 */

public interface IPresenterFactory<V, P extends RetainedPresenter<V>> {

    public P create();
}
