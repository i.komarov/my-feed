package net.styleru.i_komarov.myfeed.presenter.cache;

import android.content.Context;
import android.content.Loader;

import net.styleru.i_komarov.myfeed.common.presenter.RetainedPresenter;

/**
 * Created by i_komarov on 26.11.16.
 */

public class PresenterLoader<V, P extends RetainedPresenter<V>> extends Loader<P> {

    private final IPresenterFactory<V, P> factory;
    private P presenter;

    public PresenterLoader(Context context, IPresenterFactory<V, P> factory) {
        super(context);
        this.factory = factory;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(presenter == null) {
            forceLoad();
        } else {
            deliverResult(presenter);
        }
    }

    @Override
    public void deliverResult(P presenter) {
        this.presenter = presenter;

        if (isStarted()) {
            super.deliverResult(presenter);
        }
    }

    @Override
    protected void onForceLoad() {
        presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        onStopLoading();
        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }
}
