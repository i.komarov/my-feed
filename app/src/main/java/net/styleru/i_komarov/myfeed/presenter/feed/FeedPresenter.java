package net.styleru.i_komarov.myfeed.presenter.feed;

import android.util.Log;

import net.styleru.i_komarov.myfeed.common.presenter.BasePresenter;
import net.styleru.i_komarov.myfeed.common.presenter.RetainedPresenter;
import net.styleru.i_komarov.myfeed.view.feed.IFeedView;
import net.styleru.i_komarov.myfeed.view.feed.MockInfo;
import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;
import net.styleru.i_komarov.myfeed.view_state.list.State;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by i_komarov on 25.11.16.
 */

public class FeedPresenter extends RetainedPresenter<IFeedView> implements IFeedPresenter {

    private Scheduler io;

    private DisposableSubscriber<State> listStateSubscriber;

    public FeedPresenter() {
        io = Schedulers.io();
        Log.d("FeedPresenter", "created");
    }

    @Override
    public Flowable<List<FeedItemViewEntity>> subscribeOnListStateChannelAndGetSourceChannel(Flowable<State> stateChannel) {
        return Flowable.create(
                    new FlowableOnSubscribe<List<FeedItemViewEntity>>() {
                        @Override
                        public void subscribe(FlowableEmitter<List<FeedItemViewEntity>> feedItemChunksEmitter) throws Exception {
                            FeedPresenter.this.buildListStateSibscriber(feedItemChunksEmitter);
                            stateChannel.subscribeOn(io)
                                    .subscribe(
                                            FeedPresenter.this.listStateSubscriber
                                    );
                        }
                    },
                    BackpressureStrategy.BUFFER
                )
                //TODO: remove after using server for purpose of real items loading process
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(io)
                .observeOn(AndroidSchedulers.mainThread());
    }

    private List<FeedItemViewEntity> mockItems(State state) {
        return MockInfo.mockFeedItems(state.getOffset(), state.getLimit());
    }

    private void buildListStateSibscriber(FlowableEmitter<List<FeedItemViewEntity>> feedItemChunksEmitter) {
        this.listStateSubscriber = new DisposableSubscriber<State>() {
            @Override
            public void onNext(State state) {
                //TODO: implement normal request here as api is made
                feedItemChunksEmitter.onNext(mockItems(state));
            }

            @Override
            public void onError(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {
        if(!this.listStateSubscriber.isDisposed()) {
            this.listStateSubscriber.dispose();
        }
    }

    @Override
    public void onDestroy() {
        if(!this.listStateSubscriber.isDisposed()) {
            this.listStateSubscriber.dispose();
        }
        if(this.view != null) {
            this.view = null;
        }
    }

    @Override
    public void unbindView() {
        super.unbindView();
        /*if(!this.listStateSubscriber.isDisposed()) {
            this.listStateSubscriber.dispose();
        }*/
    }
}
