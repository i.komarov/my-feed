package net.styleru.i_komarov.myfeed.presenter.feed;

import net.styleru.i_komarov.myfeed.common.presenter.IBasePresenter;
import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;
import net.styleru.i_komarov.myfeed.view_state.list.State;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by i_komarov on 24.11.16.
 */

public interface IFeedPresenter {

    Flowable<List<FeedItemViewEntity>> subscribeOnListStateChannelAndGetSourceChannel(Flowable<State> observable);
}
