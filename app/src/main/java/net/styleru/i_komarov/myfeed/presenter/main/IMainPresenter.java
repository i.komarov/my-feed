package net.styleru.i_komarov.myfeed.presenter.main;

import net.styleru.i_komarov.myfeed.view_state.list.State;

/**
 * Created by i_komarov on 23.11.16.
 */

public interface IMainPresenter {

    void followUser();

    void loadUserData();
}
