package net.styleru.i_komarov.myfeed.presenter.main;

import net.styleru.i_komarov.myfeed.common.presenter.BasePresenter;
import net.styleru.i_komarov.myfeed.view.main.IMainActivity;

/**
 * Created by i_komarov on 23.11.16.
 */

public class MainPresenter extends BasePresenter<IMainActivity> implements IMainPresenter {

    public MainPresenter(IMainActivity view) {
        super();
    }

    @Override
    public void followUser() {

    }

    @Override
    public void loadUserData() {

    }
}
