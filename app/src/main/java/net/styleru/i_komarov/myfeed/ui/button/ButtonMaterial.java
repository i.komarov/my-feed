package net.styleru.i_komarov.myfeed.ui.button;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import net.styleru.i_komarov.myfeed.R;

/**
 * Created by i_komarov on 22.11.16.
 */

public class ButtonMaterial extends AppCompatButton {
    public ButtonMaterial(Context context) {
        super(context);
        init(context);
    }

    public ButtonMaterial(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ButtonMaterial(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_size_button));
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf"));
        setAllCaps(true);
    }
}
