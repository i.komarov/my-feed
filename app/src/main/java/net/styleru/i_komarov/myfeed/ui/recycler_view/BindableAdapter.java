package net.styleru.i_komarov.myfeed.ui.recycler_view;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.myfeed.view_state.list.State;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.functions.BiPredicate;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by i_komarov on 23.11.16.
 */

public class BindableAdapter<T> extends RecyclerView.Adapter<BindableViewHolder<T, BindableViewHolder.ActionListener<T>>> implements IAdapterLifecycle, Parcelable {

    @LayoutRes
    private final int itemRes;
    private final Class<? extends BindableViewHolder<T, BindableViewHolder.ActionListener<T>>> holderClass;

    private List<T> items;
    private BindableViewHolder.ActionListener<T> actionListener;

    private DisposableSubscriber<List<T>> sourceChannelSubscriber;

    private RecyclerView.OnScrollListener paginationListener;

    private RecyclerView recyclerView;

    private int limit = 25;

    public BindableAdapter(int itemRes, Class<? extends BindableViewHolder<T, BindableViewHolder.ActionListener<T>>> holderClass) {
        this.itemRes = itemRes;
        this.holderClass = holderClass;
        this.items = new ArrayList<>();
        this.setHasStableIds(false);
    }

    private BindableAdapter(Parcel in) throws ClassNotFoundException {
        in.readList(this.items, List.class.getClassLoader());
        this.holderClass = (Class<? extends BindableViewHolder<T, BindableViewHolder.ActionListener<T>>>) Class.forName(in.readString());
        this.itemRes = in.readInt();
        this.setHasStableIds(false);
    }

    @Override
    public BindableViewHolder<T, BindableViewHolder.ActionListener<T>> onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            return holderClass.getConstructor(View.class).newInstance(
                    LayoutInflater.from(parent.getContext()).inflate(
                            itemRes,
                            parent,
                            false
                    )
            );
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(BindableViewHolder<T, BindableViewHolder.ActionListener<T>> holder, int position) {
        holder.bind(
                position,
                items.get(position),
                actionListener
        );
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public synchronized void addItems(List<T> items) {
        final int startPosition = this.items.size();
        final int itemCount = items.size();

        this.items.addAll(items);
        notifyItemRangeInserted(startPosition, itemCount);
    }

    public synchronized boolean removeItem(int position) {
        final int startPosition = this.items.size();
        final int itemPosition = position;

        if(startPosition <= itemPosition) {
            return false;
        } else {
            this.items.remove(itemPosition);
            notifyItemRemoved(itemPosition);

            return true;
        }
    }

    public synchronized boolean removeItem(T item) {
        if(!this.items.contains(item)) {
            return false;
        } else {
            final int itemPosition = this.items.indexOf(item);
            this.items.remove(item);
            notifyItemRemoved(itemPosition);
            return true;
        }
    }

    public synchronized void addItemsToTop(List<T> items) {
        final int itemCount = items.size();
        final int startPosition = 0;
        this.items.addAll(startPosition, items);
        notifyItemRangeInserted(startPosition, itemCount);
    }

    @Override
    public synchronized void onStart() {
        sourceChannelSubscriber = new DisposableSubscriber<List<T>>() {
                    @Override
                    public void onNext(List<T> value) {
                        BindableAdapter.this.addItems(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
        };
    }

    /** Due to a memory leak issue while using this in onDestroyView of fragment, one should not use it before fix */
    @Override
    @Deprecated
    public synchronized void onDetachFromView() {
        this.recyclerView.setOnScrollListener(null);
        this.recyclerView = null;
        if(!sourceChannelSubscriber.isDisposed()) {
            sourceChannelSubscriber.dispose();
        }
    }

    @Override
    public synchronized void onStop() {
        if(this.recyclerView != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.recyclerView.setOnScrollChangeListener(null);
            }

            this.recyclerView.setAdapter(null);
            this.recyclerView.setOnScrollListener(null);
            this.recyclerView = null;
        }

        if(!this.sourceChannelSubscriber.isDisposed()) {
            this.sourceChannelSubscriber.dispose();
        }

        if(this.actionListener != null) {
            this.actionListener = null;
        }
    }

    public synchronized void clear() {
        final int itemCount = this.items.size();
        final int startPosition = 0;
        this.items.clear();
        notifyItemRangeRemoved(startPosition, itemCount);
    }

    public void setActionListener(BindableViewHolder.ActionListener<T> actionListener) {
        this.actionListener = actionListener;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public synchronized void subscribeToSourceChannel(Flowable<List<T>> dataChannel) {
        dataChannel.subscribe(sourceChannelSubscriber);
    }

    public synchronized Flowable<State> getStateObservable(final RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.setAdapter(this);

        return Flowable.create(
                    new FlowableOnSubscribe<State>() {
                        @Override
                        public void subscribe(FlowableEmitter<State> stateEmitter) throws Exception {
                            BindableAdapter.this.paginationListener = buildPaginationListener(stateEmitter);
                            BindableAdapter.this.recyclerView.setOnScrollListener(BindableAdapter.this.paginationListener);
                            BindableAdapter.this.paginationListener.onScrolled(BindableAdapter.this.recyclerView, 0, 0);
                        }
                    },
                    BackpressureStrategy.BUFFER
                )
                .distinctUntilChanged(
                        new BiPredicate<State, State>() {
                            @Override
                            public boolean test(State state1, State state2) throws Exception {
                                return state1.getOffset() == state2.getOffset() && state1.getLimit() == state2.getLimit();
                            }
                        }
                );
    }

    private State getState() {
        return new State(this.items.size(), limit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(items);
        dest.writeString(holderClass.getCanonicalName());
        dest.writeInt(itemRes);
    }

    public static final Parcelable.Creator<BindableAdapter> CREATOR = new Creator<BindableAdapter>() {
        @Override
        public BindableAdapter createFromParcel(Parcel parcel) {
            try {
                return new BindableAdapter(parcel);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException("Error while trying to create Adapter from parcel! Stacktrace: " + Arrays.toString(e.getStackTrace()));
            }
        }

        @Override
        public BindableAdapter[] newArray(int i) {
            return new BindableAdapter[0];
        }
    };

    private synchronized RecyclerView.OnScrollListener buildPaginationListener(FlowableEmitter<State> stateEmitter) {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                State state = getState();

                int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                int updatePosition = getItemCount() - 1 - (state.getLimit() / 2);

                if(updatePosition <= position) {
                    stateEmitter.onNext(state);
                }
            }
        };
    }
}
