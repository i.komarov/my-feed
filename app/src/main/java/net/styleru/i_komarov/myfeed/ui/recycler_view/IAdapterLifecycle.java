package net.styleru.i_komarov.myfeed.ui.recycler_view;

/**
 * Created by i_komarov on 25.11.16.
 */

public interface IAdapterLifecycle {

    /** Called when the adapter should listen to the source channel
     *  This is an entry point for adapter to start working
     * */
    public void onStart();

    /** Called when the view containing the adapter is coming through onDetachFromView to prevent memory leaks
     *  From this, adapter may be re-used after calling {@link IAdapterLifecycle#onStart()}}
     * */
    public void onDetachFromView();

    /** Called when the adapter should stop doing anything
     *  This is the end of adapter lifecycle, from this point it can not be re-used
     * */
    public void onStop();
}
