package net.styleru.i_komarov.myfeed.ui.text_builder_layout;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import net.styleru.i_komarov.myfeed.R;
import net.styleru.i_komarov.myfeed.ui.text_view.TextViewBody1;
import net.styleru.i_komarov.myfeed.ui.text_view.TextViewHeadline;
import net.styleru.i_komarov.myfeed.ui.text_view.TextViewSubheading;

/**
 * Created by i_komarov on 24.11.16.
 */

public class TextBuilder extends LinearLayout {

    private ParagraphBuilder builder;

    public TextBuilder(Context context) {
        super(context);
        init(context);
    }

    public TextBuilder(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TextBuilder(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public TextBuilder(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void addParagraph(ParagraphType type, String content) {
        this.addView(builder.build(type, content));
    }

    public void addParagraph(ParagraphType type, String content, Integer color) {
        AppCompatTextView view = builder.build(type, content);
        view.setTextColor(color);
        this.addView(view);
    }

    public void clear() {
        this.removeAllViews();
    }

    private void init(Context context) {
        this.builder = new ParagraphBuilder(context);
    }

    private static class ParagraphBuilder {

        private Context context;
        private int marginSmall;
        private int marginMedium;
        private int marginLarge;

        public ParagraphBuilder(Context context) {
            this.context = context;
            this.marginSmall = context.getResources().getDimensionPixelSize(R.dimen.margin_small);
            this.marginMedium = context.getResources().getDimensionPixelSize(R.dimen.margin_medium);
            this.marginLarge = context.getResources().getDimensionPixelSize(R.dimen.margin_large);
        }

        public AppCompatTextView build(ParagraphType type, String content) {
            return setContent(buildView(type), content);
        }

        private AppCompatTextView buildView(ParagraphType type) {
            AppCompatTextView view = null;

            switch(type) {
                case TITLE: {
                    view = new TextViewHeadline(context);
                    view.setPadding(marginMedium, marginLarge, marginMedium, marginLarge);
                    break;
                }
                case SUBTITLE: {
                    view = new TextViewSubheading(context);
                    view.setPadding(marginMedium, marginMedium, marginMedium, marginMedium);
                    break;
                }
                case CONTENT: {
                    view = new TextViewBody1(context);
                    view.setPadding(marginMedium, marginSmall, marginMedium, marginSmall);
                    break;
                }
            }

            view.setTextAlignment(TEXT_ALIGNMENT_VIEW_START);

            return view;
        }

        private AppCompatTextView setContent(AppCompatTextView view, String content) {
            view.setText(content);
            return view;
        }
    }

    public enum ParagraphType {
        TITLE,
        SUBTITLE,
        CONTENT
    }
}
