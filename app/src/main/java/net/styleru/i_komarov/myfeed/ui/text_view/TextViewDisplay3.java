package net.styleru.i_komarov.myfeed.ui.text_view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.TypedValue;

import net.styleru.i_komarov.myfeed.R;

/**
 * Created by i_komarov on 22.11.16.
 */

public class TextViewDisplay3 extends AppCompatTextView {

    public TextViewDisplay3(Context context) {
        super(context);
        init(context);
    }

    public TextViewDisplay3(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TextViewDisplay3(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.text_size_display_3));
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf"));
    }
}


