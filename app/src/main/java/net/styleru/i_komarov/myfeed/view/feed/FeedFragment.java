package net.styleru.i_komarov.myfeed.view.feed;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import net.styleru.i_komarov.myfeed.R;
import net.styleru.i_komarov.myfeed.common.fragment.BasePresenterFragment;
import net.styleru.i_komarov.myfeed.presenter.cache.IPresenterFactory;
import net.styleru.i_komarov.myfeed.presenter.feed.FeedPresenter;
import net.styleru.i_komarov.myfeed.presenter.feed.IFeedPresenter;
import net.styleru.i_komarov.myfeed.ui.recycler_view.BindableAdapter;
import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by i_komarov on 24.11.16.
 */

public class FeedFragment extends BasePresenterFragment<IFeedView, FeedPresenter> implements IFeedView {

    public static final String TAG = "net.styleru.i_komarov.myfeed.FeedFragment";

    private static final int FEED_PRESENTER_LOADER_ID = 8890;

    private static final String FEED_LIST_STATE = "net.styleru.i_komarov.myfeed.FEED_LIST_STATE";
    private static final String FEED_LIST_ADAPTER = "net.styleru.i_komarov.myfeed.FEED_LIST_ADAPTER";

    private BindableAdapter<FeedItemViewEntity> feedAdapter;

    private RecyclerView list;

    private Parcelable listState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feed, parent, false);
    }

    @Override
    protected void setupViewComponents(View view) {
        list = (RecyclerView) view.findViewById(R.id.fragment_feed_recycler_view);
        list.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getScrollableContentVisibleHeight(view.getContext())));
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            if (savedInstanceState.containsKey(FEED_LIST_STATE)) {
                listState = savedInstanceState.getParcelable(FEED_LIST_STATE);
            }

            if (savedInstanceState.containsKey(FEED_LIST_ADAPTER)) {
                feedAdapter = savedInstanceState.getParcelable(FEED_LIST_ADAPTER);
            }

            if(feedAdapter == null) {
                feedAdapter = new BindableAdapter<>(R.layout.list_item_feed, FeedItemViewHolder.class);
            }
        }

        if(feedAdapter == null) {
            feedAdapter = new BindableAdapter<>(R.layout.list_item_feed, FeedItemViewHolder.class);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(FEED_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
        outState.putParcelable(FEED_LIST_ADAPTER, feedAdapter);
    }

    @Override
    protected void onPresenterInitialized() {
        feedAdapter.onStart();
        feedAdapter.subscribeToSourceChannel(this.presenter.subscribeOnListStateChannelAndGetSourceChannel(feedAdapter.getStateObservable(list)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(feedAdapter != null) {
            feedAdapter.onStop();
        }
    }

    @Override
    protected IFeedView getIView() {
        return this;
    }

    @Override
    protected int getPresenterLoaderId() {
        return FEED_PRESENTER_LOADER_ID;
    }

    @Override
    protected IPresenterFactory<IFeedView, FeedPresenter> getPresenterFactory() {
        return factory;
    }

    protected int getScrollableContentVisibleHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);

        return metrics.heightPixels;
    }

    private static IPresenterFactory<IFeedView, FeedPresenter> factory = new IPresenterFactory<IFeedView, FeedPresenter>() {
        @Override
        public FeedPresenter create() {
            return new FeedPresenter();
        }
    };
}
