package net.styleru.i_komarov.myfeed.view.feed;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.styleru.i_komarov.myfeed.R;
import net.styleru.i_komarov.myfeed.ui.recycler_view.BindableViewHolder;
import net.styleru.i_komarov.myfeed.ui.text_builder_layout.TextBuilder;
import net.styleru.i_komarov.myfeed.view_object.FeedItemTagViewEntity;
import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;
import net.styleru.i_komarov.myfeed.view_object.ParagraphViewEntity;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 24.11.16.
 */

public class FeedItemViewHolder extends BindableViewHolder<FeedItemViewEntity, BindableViewHolder.ActionListener<FeedItemViewEntity>> {

    @BindView(R.id.list_item_feed_avatar)
    CircleImageView avatar;
    @BindView(R.id.list_item_feed_fullname)
    AppCompatTextView fullName;
    @BindView(R.id.list_item_feed_content)
    TextBuilder articleContent;
    @BindView(R.id.list_item_feed_images)
    GridLayout imagesLayout;
    @BindView(R.id.list_item_feed_tags)
    TextBuilder articleTags;
    @BindView(R.id.list_item_feed_more)
    AppCompatButton moreButton;

    private BitmapRequestBuilder<String, Bitmap> imageLoader;
    private BitmapRequestBuilder<String, Bitmap> avatarLoader;

    public FeedItemViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);

        imageLoader = Glide.with(itemView.getContext().getApplicationContext())
                .fromString()
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .placeholder(R.color.colorPrimaryLight)
                .dontAnimate();

        avatarLoader = Glide.with(itemView.getContext().getApplicationContext())
                .fromString()
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .skipMemoryCache(true)
                .placeholder(R.color.colorPrimaryLight)
                .dontAnimate();
    }

    @Override
    public void bind(int position, FeedItemViewEntity item, ActionListener<FeedItemViewEntity> listener) {
        super.bind(position, item, listener);

        articleContent.clear();

        imagesLayout.removeAllViews();

        avatarLoader.load(item.getSmallImageUrl())
                .fitCenter()
                .into(avatar);

        fullName.setText(item.getFirstName() + " " + item.getLastName());

        articleContent.addParagraph(TextBuilder.ParagraphType.TITLE, item.getTitle());

        for(ParagraphViewEntity paragraph : item.getParagraphs()) {
            articleContent.addParagraph(TextBuilder.ParagraphType.SUBTITLE, paragraph.getSubtitle());
            articleContent.addParagraph(TextBuilder.ParagraphType.CONTENT, paragraph.getContent());
        }

        for(FeedItemTagViewEntity tag : item.getTags()) {
            articleTags.addParagraph(TextBuilder.ParagraphType.CONTENT, tag.getContent(), tag.getColor());
        }
    }
}
