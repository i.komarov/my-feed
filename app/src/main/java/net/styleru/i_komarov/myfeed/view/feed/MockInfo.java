package net.styleru.i_komarov.myfeed.view.feed;

import net.styleru.i_komarov.myfeed.view_object.FeedItemTagViewEntity;
import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;
import net.styleru.i_komarov.myfeed.view_object.ParagraphViewEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 24.11.16.
 */

public class MockInfo {

    private static final String MOCK_AVATAR = "http://www.melbourneresumes.com.au/wp-content/uploads/best-linkedin-profile-examples-300x200.jpg";
    private static final String MOCK_FIRST_NAME = "Igor";
    private static final String MOCK_LAST_NAME = "Komarov";
    private static final String MOCK_TITLE_PREFIX = "Title ";

    private static final List<ParagraphViewEntity> MOCK_PARAGRAPHS = new ArrayList<>();
    private static final List<String> MOCK_IMAGE_URLS = new ArrayList<>();
    private static final List<FeedItemTagViewEntity> MOCK_TAGS = new ArrayList<>();

    private static final List<FeedItemViewEntity> MOCK_FEED_ITEMS = new ArrayList<>();

    public static List<FeedItemViewEntity> mockFeedItems(int offset, int limit) {

        MOCK_PARAGRAPHS.clear();
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 5", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));
        MOCK_PARAGRAPHS.add(new ParagraphViewEntity("Paragraph 6", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget efficitur est. In et malesuada dolor. Phasellus in massa nibh. Aliquam erat volutpat. Nullam iaculis ipsum in nunc iaculis, id dictum libero ultricies. Sed sodales felis sit amet pulvinar scelerisque. Pellentesque at ligula semper, faucibus massa elementum, suscipit enim. Fusce commodo nulla sed magna porttitor, vestibulum scelerisque nisl maximus. Vestibulum porta semper libero, eget tempor ipsum vestibulum ut. Duis nisi lorem, faucibus in lectus in, volutpat laoreet purus. Quisque vel malesuada ligula, ac vestibulum risus. Sed molestie leo sed magna ultricies convallis. Morbi pharetra feugiat tempus."));

        MOCK_TAGS.clear();
        MOCK_TAGS.add(new FeedItemTagViewEntity(0l, 000000, "Tag 1"));
        MOCK_TAGS.add(new FeedItemTagViewEntity(1l, 100000, "Tag 2"));
        MOCK_TAGS.add(new FeedItemTagViewEntity(2l, 001000, "Tag 3"));
        MOCK_TAGS.add(new FeedItemTagViewEntity(3l, 000010, "Tag 4"));
        MOCK_TAGS.add(new FeedItemTagViewEntity(4l, 101010, "Tag 5"));

        MOCK_IMAGE_URLS.clear();
        MOCK_IMAGE_URLS.add("http://assets.barcroftmedia.com.s3-website-eu-west-1.amazonaws.com/assets/images/recent-images-11.jpg");
        MOCK_IMAGE_URLS.add("http://7606-presscdn-0-74.pagely.netdna-cdn.com/wp-content/uploads/2016/03/Dubai-Photos-Images-Oicture-Dubai-Landmarks-800x600.jpg");
        MOCK_IMAGE_URLS.add("http://7606-presscdn-0-74.pagely.netdna-cdn.com/wp-content/uploads/2016/03/Dubai-Photos-Images-Travel-Tourist-Images-Pictures-800x600.jpg");

        MOCK_FEED_ITEMS.clear();

        for(int i = offset; i < offset + limit; i++) {
            MOCK_FEED_ITEMS.add(new FeedItemViewEntity((long) i, MOCK_AVATAR, MOCK_FIRST_NAME, MOCK_LAST_NAME, MOCK_TITLE_PREFIX + i, MOCK_PARAGRAPHS, MOCK_IMAGE_URLS, MOCK_TAGS));
        }

        return MOCK_FEED_ITEMS;
    }
}
