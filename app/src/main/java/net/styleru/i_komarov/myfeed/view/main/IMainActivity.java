package net.styleru.i_komarov.myfeed.view.main;

import net.styleru.i_komarov.myfeed.view_object.FeedItemViewEntity;
import net.styleru.i_komarov.myfeed.view_object.UserViewEntity;

import java.util.List;

/**
 * Created by i_komarov on 23.11.16.
 */

public interface IMainActivity {

    void displayUserData(UserViewEntity entity);
}
