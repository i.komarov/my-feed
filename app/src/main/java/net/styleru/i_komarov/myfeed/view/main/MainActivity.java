package net.styleru.i_komarov.myfeed.view.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatTextView;

import android.util.Log;
import android.widget.ImageView;

import net.styleru.i_komarov.myfeed.R;
import net.styleru.i_komarov.myfeed.common.activity.BaseActivity;
import net.styleru.i_komarov.myfeed.model.dto.TransactionEntity;
import net.styleru.i_komarov.myfeed.model.dto.TransactionEvent;
import net.styleru.i_komarov.myfeed.model.ipc.TransactionCallback;
import net.styleru.i_komarov.myfeed.model.ipc.TransactionListener;
import net.styleru.i_komarov.myfeed.model.service.TransactionService;
import net.styleru.i_komarov.myfeed.presenter.main.IMainPresenter;
import net.styleru.i_komarov.myfeed.view.feed.FeedFragment;
import net.styleru.i_komarov.myfeed.view_object.UserViewEntity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 22.11.16.
 */

public class MainActivity extends BaseActivity implements IMainActivity {

    private IMainPresenter presenter;

    private CircleImageView imageAvatar;
    private ImageView imageBackground;
    private AppCompatTextView textFullname;
    private AppCompatTextView textSocialNetworkTag;
    private FloatingActionButton actionButtonFollow;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("MainActivity", "service connected, name: " + name);

            try {
                TransactionListener.Stub.asInterface(service).addCallback(new TransactionCallback.Stub() {
                    @Override
                    public void onTransactionEvent(TransactionEvent transactionEvent) throws RemoteException {
                        Log.d("MainActivity", "onTransactionEvent: " + transactionEvent);
                    }
                }, 1L);
                TransactionListener.Stub.asInterface(service).executeTransaction(new TransactionEntity(2L, "some dummy content"), 1L);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("MainActivity", "service disconnected, name: " + name);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getFragmentManager().findFragmentByTag(FeedFragment.TAG) == null) {
            getFragmentManager().beginTransaction().replace(R.id.activity_main_fragment_container, new FeedFragment(), FeedFragment.TAG)
                    .commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.activity_main_fragment_container, getFragmentManager().findFragmentByTag(FeedFragment.TAG)).commit();
        }
    }

    @Override
    public void displayUserData(UserViewEntity entity) {

    }

    @Override
    public void onResume() {
        super.onResume();
        Intent transactionServiceIntent = new Intent(this.getApplicationContext(), TransactionService.class);
        bindService(transactionServiceIntent, connection, BIND_IMPORTANT);
        Log.d("MainActivity", "onResume, bindService was called");
    }

    @Override
    public void onPause() {
        unbindService(connection);
        Log.d("MainActivity", "onPause, unbindService was called");
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void setupViewComponents(Context context) {
        setContentView(R.layout.activity_main);
        imageAvatar = (CircleImageView) findViewById(R.id.activity_main_layout_profile_avatar);
        imageBackground = (ImageView) findViewById(R.id.activity_main_layout_profile_background);
        textFullname = (AppCompatTextView) findViewById(R.id.activity_main_layout_profile_data_host_fullname);
        textSocialNetworkTag = (AppCompatTextView) findViewById(R.id.activity_main_layout_profile_data_host_social_tag);
        actionButtonFollow = (FloatingActionButton) findViewById(R.id.activity_main_floating_action_button_follow);
    }
}
