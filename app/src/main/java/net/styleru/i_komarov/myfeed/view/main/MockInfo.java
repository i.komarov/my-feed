package net.styleru.i_komarov.myfeed.view.main;

/**
 * Created by i_komarov on 29.11.16.
 */

public class MockInfo {

    private static final Long MOCK_ID = 1L;
    private static final String MOCK_FIRST_NAME = "Игорь";
    private static final String MOCK_LAST_NAME = "Комаров";
    private static final String MOCK_SOCIAL_TAG = "";
}
