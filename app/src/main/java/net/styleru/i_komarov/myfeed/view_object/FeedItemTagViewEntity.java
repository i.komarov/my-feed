package net.styleru.i_komarov.myfeed.view_object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 23.11.16.
 */

public class FeedItemTagViewEntity implements Parcelable {

    private Long id;
    private int color;
    private String content;

    public FeedItemTagViewEntity(Parcel in) {
        id = in.readLong();
        color = in.readInt();
        content = in.readString();
    }

    public FeedItemTagViewEntity(Long id, int color, String content) {
        this.id = id;
        this.color = color;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "FeedItemTagViewEntity{" +
                "id='" + id + '\'' +
                ", color='" + color + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeInt(color);
        parcel.writeString(content);
    }

    public static Parcelable.Creator<FeedItemTagViewEntity> CREATOR = new Creator<FeedItemTagViewEntity>() {
        @Override
        public FeedItemTagViewEntity createFromParcel(Parcel parcel) {
            return new FeedItemTagViewEntity(parcel);
        }

        @Override
        public FeedItemTagViewEntity[] newArray(int i) {
            return new FeedItemTagViewEntity[0];
        }
    };
}
