package net.styleru.i_komarov.myfeed.view_object;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by i_komarov on 23.11.16.
 */

public class FeedItemViewEntity implements Parcelable {

    private Long id;
    private String smallImageUrl;
    private String firstName;
    private String lastName;
    private String title;
    private List<ParagraphViewEntity> paragraphs;
    private List<String> imageUrls;
    private List<FeedItemTagViewEntity> tags;

    public FeedItemViewEntity(Parcel in) {
        id = in.readLong();
        smallImageUrl = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        title = in.readString();
        in.readList(paragraphs, List.class.getClassLoader());
        in.readStringList(imageUrls);
        in.readList(tags, List.class.getClassLoader());
    }

    public FeedItemViewEntity(Long id, String smallImageUrl, String firstName, String lastName,
                              String title, List<ParagraphViewEntity> paragraphs, List<String> imageUrls, List<FeedItemTagViewEntity> tags) {
        this.id = id;
        this.smallImageUrl = smallImageUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.paragraphs = paragraphs;
        this.imageUrls = imageUrls;
        this.tags = tags;
    }

    public String getSmallImageUrl() {
        return smallImageUrl;
    }

    public void setSmallImageUrl(String smallImageUrl) {
        this.smallImageUrl = smallImageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<FeedItemTagViewEntity> getTags() {
        return tags;
    }

    public void setTags(List<FeedItemTagViewEntity> tags) {
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ParagraphViewEntity> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<ParagraphViewEntity> paragraphs) {
        this.paragraphs = paragraphs;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(smallImageUrl);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(title);
        parcel.writeList(paragraphs);
        parcel.writeStringList(imageUrls);
        parcel.writeList(tags);
    }

    public static final Parcelable.Creator<FeedItemViewEntity> CREATOR = new Creator<FeedItemViewEntity>() {
        @Override
        public FeedItemViewEntity createFromParcel(Parcel parcel) {
            return new FeedItemViewEntity(parcel);
        }

        @Override
        public FeedItemViewEntity[] newArray(int i) {
            return new FeedItemViewEntity[0];
        }
    };
}
