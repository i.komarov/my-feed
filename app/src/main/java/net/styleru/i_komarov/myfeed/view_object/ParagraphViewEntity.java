package net.styleru.i_komarov.myfeed.view_object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 24.11.16.
 */

public class ParagraphViewEntity implements Parcelable {

    private String subtitle;
    private String content;

    public ParagraphViewEntity(Parcel in) {
        subtitle = in.readString();
        content = in.readString();
    }

    public ParagraphViewEntity(String subtitle, String content) {
        this.subtitle = subtitle;
        this.content = content;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ParagraphViewEntity{" +
                "subtitle='" + subtitle + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(subtitle);
        parcel.writeString(content);
    }

    public static Parcelable.Creator<ParagraphViewEntity> CREATOR = new Creator<ParagraphViewEntity>() {
        @Override
        public ParagraphViewEntity createFromParcel(Parcel parcel) {
            return new ParagraphViewEntity(parcel);
        }

        @Override
        public ParagraphViewEntity[] newArray(int i) {
            return new ParagraphViewEntity[0];
        }
    };
}
