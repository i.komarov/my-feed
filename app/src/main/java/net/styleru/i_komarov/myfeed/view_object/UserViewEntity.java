package net.styleru.i_komarov.myfeed.view_object;

/**
 * Created by i_komarov on 23.11.16.
 */

public class UserViewEntity {

    private Long id;
    private String firstName;
    private String lastName;
    private String socialNetworkTag;
    private String smallImageUrl;
    private String mediumImageUrl;
    private String largeImageUrl;

    public UserViewEntity() {

    }

    public UserViewEntity(Long id, String firstName, String lastName, String socialNetworkTag, String smallImageUrl, String mediumImageUrl, String largeImageUrl) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialNetworkTag = socialNetworkTag;
        this.smallImageUrl = smallImageUrl;
        this.mediumImageUrl = mediumImageUrl;
        this.largeImageUrl = largeImageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialNetworkTag() {
        return socialNetworkTag;
    }

    public void setSocialNetworkTag(String socialNetworkTag) {
        this.socialNetworkTag = socialNetworkTag;
    }

    public String getSmallImageUrl() {
        return smallImageUrl;
    }

    public void setSmallImageUrl(String smallImageUrl) {
        this.smallImageUrl = smallImageUrl;
    }

    public String getMediumImageUrl() {
        return mediumImageUrl;
    }

    public void setMediumImageUrl(String mediumImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
    }

    public String getLargeImageUrl() {
        return largeImageUrl;
    }

    public void setLargeImageUrl(String largeImageUrl) {
        this.largeImageUrl = largeImageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserViewEntity{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialNetworkTag='" + socialNetworkTag + '\'' +
                ", smallImageUrl='" + smallImageUrl + '\'' +
                ", mediumImageUrl='" + mediumImageUrl + '\'' +
                ", largeImageUrl='" + largeImageUrl + '\'' +
                '}';
    }
}
