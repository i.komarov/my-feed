package net.styleru.i_komarov.myfeed.view_state.list;

/**
 * Created by i_komarov on 23.11.16.
 */

public class State {

    private int offset;
    private int limit;

    public State(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "State{" +
                "offset=" + offset +
                ", limit=" + limit +
                '}';
    }
}
